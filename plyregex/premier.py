import ply.lex as lex

tokens = ('NUMBER' , 'PLUS', 'MINUS', )
t_PLUS = r'\+'
t_MINUS = r'-'

def t_NUMBER( t ):
	r'\d+'
	t.value  = int(t.value)
	return t

def t_newline(t):
	r'\n+'
	t.lexer.lineno += len(t.value)

def t_error(t):
	print("Illegalcharacter '%s'" %t.value[0])
	t.lexer.skip(1)

t_ignore = ' |\t'

if __name__ == "__main__":
	import sys
	lexer = lex.lex()
	lexer.input(sys.stdin.read())
	for token in lexer:
		print("line %d : %s (%s) " %(token.lineno,token.type,token.value))


