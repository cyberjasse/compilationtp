import ply.lex as lex

tokens = ('CODE','BLANC','ENDLINE','VIRGULE','POINT')
states = (('inTexte','inclusive'),('afterVirgule','inclusive'),('afterPoint','inclusive'),)

def t_BLANC(t):
	r'[\t ]+'
	t.lexer.begin('inTexte')
	return t

def t_VIRGULE(t):
	r',|;|:'
	t.lexer.begin('afterVirgule')
	t.value = t.value+str(' ')
	return t

def t_POINT(t):
	r'\.|!|\?'
	t.lexer.begin('afterPoint')
	return t

def t_ENDLINE(t):
	r'[\t ]*\n'
	t.lexer.lineno += len(t.value)
	t.lexer.begin('INITIAL')
	t.value ='\n'
	return t

def t_inTexte_BLANC(t):
	r'[\t ]+'
	t.value = ' '
	return t

def t_afterPoint_BLANC(t):
	r'[\t ]+'
	t.value = ' '
	return t

def t_afterVirgule_BLANC(t):
	r'[\t ]+'
	t.value = ''
	t.lexer.begin('inTexte')
	return t

t_CODE = r'.'

def t_afterPoint_CODE(t):
	r'.'
	t.value = t.value.upper()
	t.lexer.begin('inTexte')
	return t

def t_afterVirgule_CODE(t):
	r'.'
	t.lexer.begin('inTexte')
	return t

if __name__ == "__main__":
	import sys
	lexer = lex.lex()
	lexer.input(sys.stdin.read())
	ret = ""
	lineno = 1
	for token in lexer:
		ret += token.value
	print(ret)
