import ply.lex as lex

tokens = ('COMMENTBEGIN','COMMENTEND','CODE',)
states = (('inComment','inclusive'),)

def t_inComment_COMMENTEND(t):
	r'\*/'
	t.lexer.begin('INITIAL')
	return t

def t_inComment_CODE(t):
	r'.|\n'
	if(t.value != '\n'):
		t.value = ''
	return t

def t_COMMENTBEGIN(t):
	r'/\*'
	t.lexer.begin('inComment')
	return t

t_CODE = r'.|\n'

def t_inComment_newline(t):
	r'\n+'
	t.lexer.lineno += len(t.value)

def t_newline(t):
	r'\n+'
	t.lexer.lineno += len(t.value)

if __name__ == "__main__":
	import sys
	lexer = lex.lex()
	lexer.input(sys.stdin.read())
	ret = ""
	lineno = 1
	for token in lexer:
		"""if token.lineno > lineno:
			ret += '\n' * (token.lineno - lineno)
			lineno = token.lineno"""
		if token.type == 'CODE':
			ret += token.value
	print(ret)
