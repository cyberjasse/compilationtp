import ply.lex as lex

tokens = ('TEXT','B','BEND','I','IEND','U','UEND','S','SEND','URL')
states = (('inURL','inclusive'),)
url = ''

def t_error(t):
	print("Illegalcharacter '%s'" %t.value[0])
	t.lexer.skip(1)
def t_B(t):
	r'\[b\]'
	t.value = '<strong>'
	return t
def t_BEND(t):
	r'\[/b\]'
	t.value = '</strong>'
	return t
def t_I(t):
	r'\[i\]'
	t.value = '<em>'
	return t
def t_IEND(t):
	r'\[/i\]'
	t.value = '</em>'
	return t
def t_U(t):
	r'\[u\]'
	t.value = '<ins>'
	return t
def t_UEND(t):
	r'\[/u\]'
	t.value = '</ins>'
	return t
def t_S(t):
	r'\[s\]'
	t.value = '<del>'
	return t
def t_SEND(t):
	r'\[/s\]'
	t.value = '</del>'
	return t

def t_URL(t):
	r'\[url\]'
	t.value = ''
	t.lexer.begin('inURL')
	return t

def t_inURL_URL(t):
	r'\[/url\]'
	t.value = '<a href="'+url+'">'+url+'</a>'
	t.lexer.begin('INITIAL')
	return t

t_TEXT = r'.|\n'

def t_inURL_TEXT(t):
	r'.'
	global url
	url = url+t.value
	t.value = ''
	return t

if __name__ == "__main__":
	import sys
	lexer = lex.lex()
	lexer.input(sys.stdin.read())
	for token in lexer:
		sys.stdout.write(token.value)
	sys.stdout.write('\n')
