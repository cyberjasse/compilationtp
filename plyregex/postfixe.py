import ply.lex as lex
from math import cos
from math import sin

tokens = ('FLOAT','INTEGER','OP','SIN','COS' )

t_SIN = r'sin'
t_COS = r'cos'

def t_FLOAT( t ):
	r'\d+\.\d+'
	t.value  = float(t.value)
	return t

def t_INTEGER( t ):
	r'\d+'
	t.value  = int(t.value)
	return t

def t_OP( t ):
	r'\+|-|\*|/'
	t.value = t.value[0]
	return t

def t_newline(t):
	r'\n+'
	t.lexer.lineno += len(t.value)

def t_error(t):
	print("Illegalcharacter '%s'" %t.value[0])
	t.lexer.skip(1)

t_ignore = ' |\t'

if __name__ == "__main__":
	import sys
	lexer = lex.lex()
	lexer.input(sys.stdin.read())
	queue = []
	for token in lexer:
		print("line %d : %s (%s) " %(token.lineno,token.type,token.value))
		if(token.type == 'FLOAT' or token.type == 'INTEGER'):
			queue.append(token.value)
		elif(token.type == 'OP'):
			val1 = queue.pop()
			val2 = queue.pop()
			if token.value=='+':
				result = val1+val2
			elif token.value=='-':
				result = val2-val1
			elif token.value=='*':
				result = val1*val2
			elif token.value=='/':
				result = val1/val2
			print(str(val2)+" "+str(val1)+" "+str(token.value)+" = "+str(result))
			queue.append(result)
		elif(token.type == 'SIN'):
			queue.append(sin(queue.pop()))
		elif(token.type == 'COS'):
			queue.append(cos(queue.pop()))
	result = queue.pop()
	print("Le resultat de la ligne est "+str(result))
