import ply.lex as lex

tokens = ('FLOAT','INTEGER','ADD_OP','MUL_OP','OPEN','CLOSE' )
t_ADD_OP = r'\+|-'
t_MUL_OP = r'\*|/'
t_OPEN = r'\('
t_CLOSE = r'\)'

def t_FLOAT( t ):
	r'\d+\.\d+'
	t.value  = float(t.value)
	return t

def t_INTEGER( t ):
	r'\d+'
	t.value  = int(t.value)
	return t

def t_newline(t):
	r'\n+'
	t.lexer.lineno += len(t.value)

def t_error(t):
	print("Illegalcharacter '%s'" %t.value[0])
	t.lexer.skip(1)

t_ignore = ' \t'

if __name__ == "__main__":
	import sys
	lexer = lex.lex()
	lexer.input(sys.stdin.read())
	ascensseur = 0
	for token in lexer:
		print("line %d : %s (%s) " %(token.lineno,token.type,token.value))
		if(token.type == 'OPEN'):
			ascensseur += 1
		elif(token.type == 'CLOSE'):
			if(ascensseur == 0):
				print("mal parenthese. Trop de parenthese fermante ?")
				exit()
			ascensseur -= 1
	if(ascensseur != 0):
		print("mal parenthese. Trop de parenthese ouvrante ?")
		exit()
