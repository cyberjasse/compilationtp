import ply.lex as lex

tokens = ('QUOTE','CHAR','ECHAP','NEWLINE')
states = (('inChaine','inclusive'),('inEchap','inclusive'),)
currentQuote = ''

def t_QUOTE(t):
	r'"|\''
	t.lexer.begin('inChaine')
	global currentQuote
	if t.value=='"':
		currentQuote = '"'
	elif t.value=="'":
		currentQuote = "'"
	else:
		print "[erreur quote]"
	t.value = ''
	return t

def t_inChaine_QUOTE(t):
	r'"|\''
	if t.value==currentQuote:
		t.lexer.begin('INITIAL')
		t.value = '\n'
	return t

def t_inChaine_ECHAP(t):
	r'\\'
	t.lexer.begin('inEchap')
	t.value = ''
	return t

def t_CHAR(t):
	r'.|\n'
	t.value = ''
	return t

def t_inEchap_CHAR(t):
	r'.'
	t.lexer.begin('inChaine')
	if t.value == 'a':
		t.value = '\a'
	elif t.value == 't':
		t.value = '\t'
	elif t.value == 'n':
		t.value = '\n'
	elif t.value == '\'':
		t.value = '\''
	elif t.value == '"':
		t.value = '"'
	elif t.value == '\\':
		t.value = '\\'
	else:
		print "caractere special inconnu: \\"+t.value
	return t

def t_inChaine_CHAR(t):
	r'.|\n'
	return t

def t_error(t):
	print("Illegalcharacter '%s'" %t.value[0])
	t.lexer.skip(1)

if __name__ == "__main__":
	import sys
	lexer = lex.lex()
	lexer.input(sys.stdin.read())
	ret = ""
	lineno = 1
	for token in lexer:
		sys.stdout.write(token.value)
	sys.stdout.write('\n')
