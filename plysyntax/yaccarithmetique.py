import ply.yacc as yacc
from lexarithmetique import tokens

def p_expression_num(p):
	'''expression : NUMBER'''
	p[0] = p[1]

def p_expression_addop(p):
	'''expression : expression ADD_OP expression'''
	if p[2] == '+':
		p[0] = p[1] + p[3]
	elif p[2] == '-':
		p[0] = p[1] - p[3]

def p_expression_mulop(p):
	'''expression : expression MUL_OP expression'''
	if p[2] == '*':
		p[0] = p[1] * p[3]
	elif p[2] == '/':
		p[0] = p[1] / p[3]

def p_expression_bracket(p):
	'''expression : OPEN expression CLOSE'''
	p[0] = p[2]

def p_error(p):
	print('Syntax error in line {}'.format(p.lineno))
	yacc.error()

def p_expr_UN(p):
	'''expression : ADD_OP expression %prec UN'''
	if p[1] == '-':
		p[0] = -p[2]
	else:
		p[0] = p[2]

precedence = (
	("left", "ADD_OP"),
	("left", "MUL_OP"),
	("right", "UN"),	#- ou + unaire
)

yacc.yacc(outputdir='generated')

if __name__ == '__main__':
	import sys
	input = file(sys.argv[1]).read()
	result = yacc.parse(input, debug=True)
	print(input)
	print result

